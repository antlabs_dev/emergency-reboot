<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Emergency reboot</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!--
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        -->
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <form action="/reboot" enctype="multipart/form-data" method="post">
            <p><input type="checkbox" name="check_1" value="check_1" /> Я хочу перезагрузить сервер</p>
            <p><input type="checkbox" name="check_2" value="check_2" /> Я не идиот и знаю зачем это делаю</p>
            <p><input type="checkbox" name="check_3" value="check_3" /> И даже могу рассказать об этом если тебе так угодно</p>
            <p><textarea rows="4" cols="50" name="text"></textarea></p>
            <input type="image" src="static/button.png" alt="Reboot!" style="max-width: 100%; max-height: 100%;"/>
       </form>
</html>
