#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import bottle
import os
import subprocess
import logging

HOST = '0.0.0.0'
PORT = 666
REBOOT_COMMAND = "reboot"
REDIRECT_AFTER_REBOOT_TO = "https://www.google.ru/search?q=%D0%BA%D0%BE%D1%82%D1%8F%D1%82%D0%B0&newwindow=1&source=lnms&tbm=isch&sa=X&ei=vC4uVd0P5_7LA_-VgIgM&ved=0CAcQ_AUoAQ&biw=1678&bih=974"

def perform_reboot():
    logging.warning("performing reboot via '{c}' command".format(c=REBOOT_COMMAND))
    try:
        process = subprocess.Popen([REBOOT_COMMAND], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = process.communicate()
        logging.info("'{c}' command stdout: {o}, stderr: {e}".format(o=out, e=err, c=REBOOT_COMMAND))
        return True if process.returncode == 0 else False
    except:
        logging.exception("error executing '{c}'".format(c=REBOOT_COMMAND))
        return False
        
@bottle.get('/static/:filename#.*#')
def send_static(filename):
    return bottle.static_file(filename, root=os.path.dirname(os.path.abspath(__file__)))

@bottle.get('/')
def index():
    return bottle.template("index.html")

@bottle.post('/reboot')
def reboot():
    if bottle.request.forms.get("check_1") and bottle.request.forms.get("check_2") and bottle.request.forms.get("check_3"):
        text = bottle.request.forms.get("text")
        if len is None or len(text) < 10:
            logging.error("too short text")
            bottle.abort(403, "Текст должен быть не менее 10 символов")
        logging.info("reason: '{r}'".format(r=text))
        if perform_reboot():
            bottle.redirect(REDIRECT_AFTER_REBOOT_TO)
        else:
            bottle.abort(500, "Что-то пошло не так, смотри лог")
    else:
        logging.error("not confirmed reboot")
        bottle.abort(403, "Необходимо отметить все пункты")

if __name__ == '__main__':
    work_dir = os.path.dirname(os.path.abspath(__file__))
    os.chdir(work_dir)
    bottle.TEMPLATE_PATH.append(work_dir)
    logging.basicConfig(filename="log", 
                        format = "[%(asctime)s] %(name)s |%(levelname)s| %(message)s", 
                        datefmt="%Y-%m-%d %H:%M:%S", 
                        level=logging.DEBUG)
    logging.info("up and running on {h}:{p}".format(h=HOST, p=PORT))
    bottle.run(host=HOST, port=PORT)